﻿using System.Collections.Generic;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectLab.Pages
{
    public class SelectFlightPage : BasePage
    {
        [FindsBy(How = How.XPath, Using = "//*[contains(@class,'4 date')]")]
        public IWebElement departureDate;

        [FindsBy(How = How.XPath, Using = "//*[@id='fare-selector-outbound']//address")]
        public IWebElement flightRoute;

        [FindsBy(How = How.XPath, Using = "//li[contains(@class,'--selected')]//time")]
        public IWebElement highlightedDate;

        [FindsBy(How = How.XPath, Using = "//div[@class='rf-fare']")]
        public IList<IWebElement> threePrices;

        [FindsBy(How = How.Id, Using = "fare-selector-return")]
        public IWebElement returnPrices;

        [FindsBy(How = How.XPath, Using = "(//div[@class='rf-fare__price'])[3]")]
        public IWebElement middleButton;

        [FindsBy(How = How.Id, Using = "flight-select-continue-btn")]
        public IWebElement continueButton;

        public SelectFlightPage(IWebDriver driver) : base(driver)
        {
        }
    }
}
