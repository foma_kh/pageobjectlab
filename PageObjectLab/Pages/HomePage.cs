﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectLab.Pages
{
    public class HomePage : BasePage
    {

        [FindsBy(How = How.Id, Using = "search-departure-station")]
        public IWebElement departureInput;

        [FindsBy(How = How.ClassName, Using = "locations-container__location")]
        public IWebElement confirmCity;

        [FindsBy(How = How.Id, Using = "search-arrival-station")]
        public IWebElement arrivalInput;

        [FindsBy(How = How.Id, Using = "search-departure-date")]
        public IWebElement selectedDate;

        [FindsBy(How = How.XPath, Using = "//button[@tabindex='2']")]
        public IWebElement searchButton;

        public HomePage(IWebDriver driver) : base(driver)
        {
        }

        public void SetDeparture(String str)
        {
            departureInput.SendKeys(str);
        }

        public void SetArrival(String str)
        {
            arrivalInput.SendKeys(str);
        }

        public void ConfirmCity()
        {
            confirmCity.Click();
        }
    }
}