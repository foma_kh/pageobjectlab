﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace PageObjectLab.Pages
{
    public class BookingPage : BasePage
    {
        [FindsBy(How = How.Id, Using = "passenger-first-name-0")]
        public IWebElement firstNameInput;

        [FindsBy(How = How.Id, Using = "passenger-last-name-0")]
        public IWebElement lastNameInput;

        [FindsBy(How = How.CssSelector, Using = "[data-test=passenger-gender-0-male]")]
        public IWebElement maleButton;

        [FindsBy(How = How.Id, Using = "passengers-continue-btn")]
        public IWebElement passengerContinueButton;

        [FindsBy(How = How.Id, Using = "login-modal")]
        public IWebElement signInPopup;

        public BookingPage(IWebDriver driver) : base(driver)
        {
        }
    }
}
