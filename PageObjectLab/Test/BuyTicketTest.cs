﻿using System;
using OpenQA.Selenium;
using NUnit.Framework;
using PageObjectLab.Test;


namespace PageObjectLab
{
    [TestFixture]
    public class BuyTicketTest : BaseTest
    {
        [SetUp]
        public void SetUp()
        {
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("http://wizzair.com");
            InitDriver();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(25);
        }

        [Test]
        public void WizzAirTest()
        {
            homePage.departureInput.Clear();
            homePage.SetDeparture(departureCity);
            homePage.ConfirmCity();
            homePage.arrivalInput.Clear();
            homePage.SetArrival(arrivalCity);
            homePage.ConfirmCity();
            String sSelectedDate = homePage.selectedDate.Text;
            String enteredDeparture = homePage.departureInput.Text;
            String enteredArrival = homePage.arrivalInput.Text;
            homePage.searchButton.Click();

            try
            {
                driver.SwitchTo().Window(driver.WindowHandles[0]);
            }
            catch (ArgumentOutOfRangeException)
            {
                driver.SwitchTo().Window(driver.WindowHandles[1]);
            }
            String sDepartureDate = selectFlightPage.departureDate.Text;
            Assert.That(sDepartureDate, Does.Contain(sSelectedDate), "The departureDay shoud be equal to selected Day");

            String sFlightRoute = selectFlightPage.flightRoute.Text;
            StringAssert.StartsWith(enteredDeparture, sFlightRoute, "The departure should be equal entered");//Checking the departure City
            StringAssert.EndsWith(enteredArrival, sFlightRoute, "The arrival should be equal entered");//Checking the arrival City

            String sHighlightedDate = selectFlightPage.highlightedDate.Text;
            DateTime date1 = DateTime.Parse(sHighlightedDate);
            DateTime date2 = DateTime.Parse(sDepartureDate);
            Assert.AreEqual(date1, date2, "The Highlighted Date should be equal to departure date"); //checking the correct date is selected
            Assert.That(selectFlightPage.threePrices.Count.Equals(3), "Only 3 prices should be displayed");//3 options with different prices are displayed including prices

            try
            {
                isVisible = selectFlightPage.returnPrices.Displayed;
            }
            catch (NoSuchElementException)
            {
                isVisible = false;
            }
            Assert.IsFalse(isVisible, "Error!Return flights are displayed");

            selectFlightPage.middleButton.Click();
            selectFlightPage.continueButton.Submit();

            bookingPage.firstNameInput.SendKeys(firstName);
            bookingPage.lastNameInput.SendKeys(lastName);
            bookingPage.maleButton.Click();
            bookingPage.passengerContinueButton.Submit();
            Assert.IsTrue(condition: bookingPage.signInPopup.Displayed, message: "Error! SignInPopup is not displayed");
        }

        [TearDown]
        public void TearDown()
        {
            driver.Close();
        }
    }
}
