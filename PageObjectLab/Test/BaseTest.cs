﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using PageObjectLab.Pages;
using System;

namespace PageObjectLab.Test
{
    public class BaseTest
    {
        public IWebDriver driver = new ChromeDriver(@"C:\ChromeDriver");
        public HomePage homePage;
        public SelectFlightPage selectFlightPage;
        public BookingPage bookingPage;
        public String departureCity = "Kiev";
        public String arrivalCity = "Copenhagen";
        public bool isVisible = false;
        public String firstName = "Antoine";
        public String lastName = "Griezmann";

        public void InitDriver()
        {
            homePage = new HomePage(driver);
            selectFlightPage = new SelectFlightPage(driver);
            bookingPage = new BookingPage(driver);
        }
    }
}